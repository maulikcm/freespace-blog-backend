import { Schema, model, Document } from 'mongoose';

export interface IBlogs extends Document {
  userId: string;
  title: string;
  content: string;
  isPublished: boolean;
}

const Blogs = new Schema<IBlogs>(
  {
    userId: { type: String },
    title: String,
    content: String,
    isPublished: { type: Boolean, default: false }
  },
  { timestamps: { createdAt: 'createdAt', updatedAt: 'modifiedAt' } }
);

Blogs.index({ userId: 1 });

export const BlogsModel = model<IBlogs>('blogs', Blogs);
