import { Schema, model, Document } from 'mongoose';

export interface IComment extends Document {
  email: string;
  blogId: string;
  content: string;
}

const Comments = new Schema<IComment>(
  {
    email: String,
    blogId: String,
    content: String
  },
  { timestamps: { createdAt: 'createdAt', updatedAt: 'modifiedAt' } }
);

Comments.index({ blogId: 1 });

export const CommentsModel = model<IComment>('comments', Comments);
