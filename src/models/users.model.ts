import { Schema, model, Document } from 'mongoose';

export interface IUser extends Document {
  userId: string;
  email: string;
}

const Users = new Schema<IUser>(
  {
    userId: { type: String, unique: true },
    email: { type: String, unique: true }
  },
  { timestamps: { createdAt: 'createdAt', updatedAt: 'modifiedAt' } }
);

Users.index({ userId: 1, email: 1 });

export const UsersModel = model<IUser>('users', Users);
