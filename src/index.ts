import express from 'express';
import dotenv from 'dotenv';
import routes from './routes';
import Mongoose, { ConnectOptions } from 'mongoose';
import { log } from './middleware';

import cors from 'cors';
const app = express();
dotenv.config();

const port = process.env.PORT || 8080;

const dbOpts: ConnectOptions = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
};

Mongoose.connect(process.env.DB, dbOpts, (err) => {
  if (err) throw err;
  console.log('Database connected successfully');
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
  methods: '*',
  allowedHeaders:
    'Origin, X-Requested-With, Content-Type, Accept, Authorization, Referer, DNT, User-Agent'
};
app.use(cors(corsOptions));
app.options('*', cors(corsOptions));
app.use(log);
app.use(routes);

app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
