import { RequestHandler } from 'express';
import admin, { ServiceAccount } from 'firebase-admin';
import { UsersModel } from './models/users.model';
import serviceAccount from './serviceAccount.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as ServiceAccount)
});

export const verify: RequestHandler = (req, res, next) => {
  const token = req.headers.authorization;
  if (token) {
    admin
      .auth()
      .verifyIdToken(token)
      .then(async (decodedToken) => {
        req.body.uid = decodedToken.uid;
        next();
      })
      .catch((error) => {
        res.status(401).send({ error });
      });
  } else {
    res.status(401).send({ error: 'Auth token not found' });
  }
};

export const log: RequestHandler = async (req, res, next) => {
  const time = new Date().getTime();
  await next();
  console.log(
    `${req.method}: ${req.originalUrl} - ${
      new Date().getTime() - time
    }ms - status: ${res.statusCode}`
  );
};
