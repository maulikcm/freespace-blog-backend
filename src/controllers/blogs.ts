import { RequestHandler } from 'express';
import { CommentsModel } from '../models/comments.model';
import { Types } from 'mongoose';
import { BlogsModel, IBlogs } from '../models/blogs.model';

export const createPost: RequestHandler = (req, res) => {
  if (req.body.title && req.body.content) {
    const newPost = new BlogsModel({
      userId: req.body.uid,
      title: req.body.title,
      content: req.body.content,
      isPublished: false
    });
    newPost.save((err, doc) => {
      if (err) {
        res.status(400).send({ success: false, error: err.message });
      } else {
        res.status(200).send({ success: true, data: doc });
      }
    });
  } else {
    res
      .status(400)
      .send({ success: false, error: 'Required fields not found' });
  }
};

export const updatePost: RequestHandler = async (req, res) => {
  if (req.body.data.title && req.body.data.content && req.body.id) {
    const blog: IBlogs = await BlogsModel.findOneAndUpdate(
      { _id: req.body.id },
      req.body.data,
      { new: true }
    );
    res.status(200).send({ success: true, data: blog });
  } else {
    res
      .status(400)
      .send({ success: false, error: 'Required fields not found' });
  }
};

export const getAllPublishedBlogs: RequestHandler = async (req, res) => {
  const blogs: IBlogs[] = await BlogsModel.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: 'userId',
        as: 'user'
      }
    },
    {
      $unwind: {
        path: '$user'
      }
    },
    { $match: { isPublished: true } }
  ]);
  res.status(200).send({ success: true, data: blogs });
};

export const getBlog: RequestHandler = async (req, res) => {
  const blogs: IBlogs[] = await BlogsModel.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: 'userId',
        as: 'user'
      }
    },
    {
      $unwind: {
        path: '$user'
      }
    },
    {
      $lookup: {
        from: 'comments',
        let: {
          bid: '$_id'
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: [
                  '$$bid',
                  {
                    $toObjectId: '$blogId'
                  }
                ]
              }
            }
          }
        ],
        as: 'comments'
      }
    },
    { $match: { _id: Types.ObjectId(req.params.id) } }
  ]);
  res.status(200).send({ success: true, data: blogs[0] });
};

export const getAllBlogsByUser: RequestHandler = async (req, res) => {
  const blogs: IBlogs[] = await BlogsModel.aggregate([
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: 'userId',
        as: 'user'
      }
    },
    {
      $unwind: {
        path: '$user'
      }
    },
    { $match: { userId: req.body.uid } }
  ]);
  res.status(200).send({ success: true, data: blogs });
};

export const publishBlog: RequestHandler = async (req, res) => {
  const blog: IBlogs = await BlogsModel.findOneAndUpdate(
    { _id: req.params.id },
    { isPublished: true },
    { new: true }
  );
  res.status(200).send({ success: true, data: blog });
};

export const deleteBlog: RequestHandler = async (req, res) => {
  await BlogsModel.findOneAndDelete({ _id: req.params.id });
  res.status(200).send({ success: true });
};

export const addComment: RequestHandler = async (req, res) => {
  if (req.body.blogId && req.body.content && req.body.email) {
    const newPost = new CommentsModel(req.body);
    newPost.save((err, doc) => {
      if (err) {
        res.status(400).send({ success: false, error: err.message });
      } else {
        res.status(200).send({ success: true, data: doc });
      }
    });
  } else {
    res
      .status(400)
      .send({ success: false, error: 'Required fields not found' });
  }
};
