import { RequestHandler } from 'express';
import { UsersModel, IUser } from '../models/users.model';

export const addUser: RequestHandler = async (req, res) => {
  if (req.body.email && req.body.userId) {
    const user: IUser = await UsersModel.findOne(req.params);
    if (user) {
      res
        .status(400)
        .send({ success: false, error: 'The user is already exists' });
    } else {
      const newUser = new UsersModel(req.body);
      newUser.save((err, doc) => {
        if (err) {
          res.status(400).send({ success: false, error: err.message });
        } else {
          res.status(200).send({ success: true, data: doc });
        }
      });
    }
  } else {
    res
      .status(400)
      .send({ success: false, error: 'Required fields not found' });
  }
};

export const getUsers: RequestHandler = async (req, res) => {
  const users: IUser[] = await UsersModel.find({});
  res.status(200).send({ success: true, data: users });
};

export const getUserById: RequestHandler = async (req, res) => {
  const user: IUser = await UsersModel.findOne(req.params);
  res.status(200).send({ success: true, data: user });
};
