import { Router } from 'express';
import userRouter from './users.routes';
import blogRouter from './blogs.routes';

const routes = Router();

routes.use('/user', userRouter);
routes.use('/blog', blogRouter);

export default routes;
