import { Router } from 'express';
import { verify } from '../middleware';
import {
  addComment,
  createPost,
  deleteBlog,
  getAllBlogsByUser,
  getAllPublishedBlogs,
  getBlog,
  publishBlog,
  updatePost
} from '../controllers/blogs';

const blogsRouter = Router();

blogsRouter.post('/', verify, createPost);
blogsRouter.patch('/', verify, updatePost);
blogsRouter.get('/', verify, getAllBlogsByUser);

blogsRouter.get('/publish/:id', verify, publishBlog);
blogsRouter.delete('/:id', verify, deleteBlog);

blogsRouter.get('/published', getAllPublishedBlogs);
blogsRouter.get('/:id', getBlog);

blogsRouter.post('/comment', addComment);

export default blogsRouter;
