import { Router } from 'express';
import { addUser, getUsers, getUserById } from '../controllers/users';

const usersRouter = Router();

usersRouter.post('/', addUser);
usersRouter.get('/', getUsers);
usersRouter.get('/:userId', getUserById);
// TODO get all blogs of user
// TODO create new blog

export default usersRouter;
